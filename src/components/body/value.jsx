import React from 'react'
import './value.css'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// const icon = <FontAwesomeIcon icon={["fal", "coffee"]} />
// import { faHome } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const values = [
    {name : "Manifold", position : "An n-dimensional manifold, or n-manifold for short, is a topological space with the property that each point has a neighborhood that is homeomorphic to the Euclidean space of dimension n."},
    {name : "Manifold", position : "An n-dimensional manifold, or n-manifold for short, is a topological space with the property that each point has a neighborhood that is homeomorphic to the Euclidean space of dimension n."},
    {name : "Manifold", position : "An n-dimensional manifold, or n-manifold for short, is a topological space with the property that each point has a neighborhood that is homeomorphic to the Euclidean space of dimension n."},
    {name : "Manifold", position : "An n-dimensional manifold, or n-manifold for short, is a topological space with the property that each point has a neighborhood that is homeomorphic to the Euclidean space of dimension n."}
];

const content = values.map((value) =>
    <div key={value.name} className='col-md-6 col-sm-6'>
        <div>
            <div id='x' className='col-md-2 col-sm-2'>
            </div>
            <div id='icon' className='col-md-4 col-sm-4'>
                {/* <i className="fas fa-user-friends" styles='font-size:6px'></i>  */}
                {/* <i class='fas fa-users' styles='font-size:48px;color:red'></i> */}
                {/* {icon} */}
                {/* <i className="fa fa-spinner fa-spin">no spinner but why</i> */}
                {/* <FontAwesomeIcon icon={faHome} /> */}
                {/* <i class="fas fa-user-friends"></i> */}
                <i class='fas fa-user-friends fa-3x'></i><br/>
                {/* <i class="fa fa-users" aria-hidden="true"></i> */}
            </div>
            <div id = 'content' className='col-md-6 col-sm-6'>
                <h4>{value.name}</h4>
                <p>{value.position}</p> 
            </div>
        </div>
    </div>
);


class Value extends React.Component{
    render(){
        return(
            <div id='value' className='text-center' >
                {/* <img src={img} alt="..." width="500" height="600"></img> */}
                <div className='container'>
                    <div className='col-md-8 col-md-offset-2'>
                        {/* <img src="images/green.jpg" alt="..." width="500" height="600"></img> */}
                        <h2>Key Value</h2>
                    </div>
                    <div id='row'>
                        {content}
                    </div>
                </div>
            </div>
        )
    }
}

export default Value;
import React from 'react'
import './member.css'


const members = [
    {name : "Luong Huu Tuan", position : "Dev quèn"},
    {name : "Pham Hoang Hung", position : "Leader"},
    {name : "Tran Trong Hiep", position : "Leader"},
    {name : "Pham Tung Lam", position : "Leader"}
];

const content = members.map((member) =>
    <div key={member.name} className='col-md-3 col-sm-6 team'>
        <div className='thumbnail'>
            <img src='images/avatar.jpg' alt='...' className='team-img' />
            <div className='caption'>
                <h4>{member.name}</h4>
                <p>{member.position}</p>
            </div>
        </div>
    </div>
);


class Member extends React.Component{
    render(){
        return(
            <div id='team' className='text-center'>
                <div className='container'>
                    <div className='col-md-8 col-md-offset-2'>
                    <h2>Key Members</h2>
                    <p> Founders </p>
                    </div>
                    <div id='row'>
                        {content}
                    </div>
                </div>
            </div>
        )
    }
}

export default Member;
import React from 'react'
// import './body.css'
import Member from './member'
import Value from './value'

class Body extends React.Component{
    render(){
        return(
            <main>
                <Value />
                <Member />
            </main>
        )
    }
}

export default Body
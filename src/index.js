import React from 'react';
import ReactDOM from "react-dom";
import Header from './components/header/header'
import Footer from './components/footer/footer'
import Body from './components/body/body'


// class Hello extends React.Component{
//     constructor(props){
//         super(props);
//         this.state = {
//             own_name: this.props.own_name
//         };
//     }
//     render() {
//         return <h2>{this.state.own_name} introduction</h2>;
//     }
// }
// ReactDOM.render(<Hello own_name='HTH team' />, document.getElementById("root"));


class App extends React.Component{
    render(){
        return (
            <div className="container">
                <Header />
                <Body />
                <Footer />
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("root"))